console.log("Typescrypt course Class");

class Department {
  static year = 2000; //static properties, can access directly without init class (New)
  // _name: string;
  // private _employees: string[] = []; // cannot modified outside class or extend classs
  protected _employees: string[] = []; // cannot modified outside class but can access in extend class

  // constructor(name: string) {//general constructor
  constructor(protected _id: string = "", private _name: string = "") {
    //short constructor
    // this._name = name;
  }

  static createEmploye(text: string) {
    //static methods, can access directly without init class (New)
    return { name: text };
  }

  describe() {
    console.log("Department " + this._id + " : " + this._name);
  }
  addEmploye(employ: string) {
    this._employees.push(employ);
  }
  getEmployee() {
    console.log(this._employees.length);
    console.log(this._employees);
  }
}

class ITDepartment extends Department {
  //inherit Depamrtment class
  _admins: string[];
  private _lastReport: string;

  get reportAdmin() {
    //getter must return value
    if (this._lastReport) {
      return this._lastReport;
    }
    throw new Error("Report not found");
  }

  set reportAdmin(value: string) {
    if (!value) {
      throw new Error("please add value");
    }
    this.addEmploye(value);
  }

  constructor(_id: string, admins: string[], report: string[]) {
    super(_id, "IT"); //call constructor of parent class
    this._admins = admins;
    this._lastReport = report[0];
  }
  describe() {
    console.log("Id It Depatement is " + this._id);
  }
  addEmploye(name: string) {
    this._employees.push(name);
  }
}
const createE = Department.createEmploye("Junaedi");
// console.log(createE, Department.year);
const dept = new Department("1", "EDP");
// console.log(dept);
const itdept = new ITDepartment("1", ["juna"], ["akan"]);
// console.log(itdept.reportAdmin);
// itdept.reportAdmin = "Mama";

// console.log(itdept);
// dept.describe();
// dept.addEmploye("juna");
// dept.addEmploye("yunus");
// itdept.addEmploye("jojsoa");
// dept.getEmployee();
itdept.describe();
