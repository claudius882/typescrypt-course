console.log("Typescrypt course object types");

// const user={
//   id: string;
//   price: number;
//   tags: string[];
//   details: {
//     title: string;
//     description: string;
//   }
// }

enum Role {
  ADMIN,
  AUTHOR,
  USER,
} //enum types

// const user: {
//   name: string;
//   age: number;
//   hobbies: string[]; //array types
//   role: [number, string]; //tuple types(fixed array dan fixed types);
// } = {
const user = {
  name: "max",
  age: 30,
  hobbies: ["makan", "mancing", "tidur"],
  role: [1, "author"],
  role1: Role.USER,
};

const aa: {
  id: number;
  name: string;
}[] = [
  { id: 1, name: "sasas" },
  { id: 1, name: "sasas" },
];

console.log(user.name);
