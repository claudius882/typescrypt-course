console.log("Typescrypt course return types");

function add(n1: number, n2: number) {
  //have return (types number)
  return n1 + n2;
}

function printResult(text: number) {
  //void return types (does not have return)
  console.log("result is: " + text);
}

// let combineValues: Function; //function types(must function values)
let combineValues: (a: number, b: number) => number; //function types return number types;

combineValues = add;

console.log(combineValues(8, 2));

function addHandler(n1: number, n2: number, cb: (num: number) => void) {
  const result = n1 + n2;
  cb(result);
}
addHandler(1, 4, (results) => {
  console.log(results);
});
printResult(add(12, 22));
