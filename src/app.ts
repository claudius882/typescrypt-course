console.log("Typescrypt course interface");

interface addFn {
  (a: number, b: number): number;
}
let adds: addFn;

adds = (n1, n2) => {
  return n1 + n2;
};
console.log(adds(1, 2));
interface Name {
  readonly name: string;
  optionalName?: string; //optional property
}
interface Greetable extends Name {
  greet(phrase: string): void;
}

class Person implements Greetable {
  name: string;

  constructor(n: string) {
    this.name = n;
  }

  greet(phrase: string) {
    console.log(phrase + " " + this.name);
  }
}

let user1: Greetable;

user1 = {
  name: "Junaedi",
  greet(phrase: string) {
    console.log(phrase + " " + this.name);
  },
};
user1 = new Person("Junaedi ramli");
user1.greet("Hai, nama saya");
// user1.name = "kaka";
