console.log("Typescrypt course union types");

type Combine = number | string; // declare Aliases/custom types

const combine = (
  input1: Combine, //aliases/custom types
  input2: number | string, //union types
  results: "as-number" | "as-text" //literal types
) => {
  let result: number | string;
  if (
    typeof input1 === "number" &&
    typeof input2 === "number" &&
    results === "as-number"
  ) {
    result = +input1 + +input2;
  } else {
    result = input1.toString() + input2.toString();
  }
  return result;
};

const ages = combine(12, 22, "as-number");
console.log(ages);
