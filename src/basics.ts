console.log("Typescrypt course basic types");

function add1(num1: number, num2: number, showResult: boolean, title: string) {
  const results = num1 + num2;
  if (showResult) {
    console.log(title + results);
  } else {
    return results;
  }
  return;
}

const number1 = 2;
const number2 = 4;
const res = true;
const title = "results is: ";
add1(number1, number2, res, title);
