"use strict";
console.log("Typescrypt course return types");
function add(n1, n2) {
    return n1 + n2;
}
function printResult(text) {
    console.log("result is: " + text);
}
let combineValues;
combineValues = add;
console.log(combineValues(8, 2));
function addHandler(n1, n2, cb) {
    const result = n1 + n2;
    cb(result);
}
addHandler(1, 4, (results) => {
    console.log(results);
});
printResult(add(12, 22));
