"use strict";
console.log("Typescrypt course union types");
const combine = (input1, input2, results) => {
    let result;
    if (typeof input1 === "number" &&
        typeof input2 === "number" &&
        results === "as-number") {
        result = +input1 + +input2;
    }
    else {
        result = input1.toString() + input2.toString();
    }
    return result;
};
const ages = combine(12, 22, "as-number");
console.log(ages);
