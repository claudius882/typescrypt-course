"use strict";
console.log("Typescrypt course interface");
let adds;
adds = (n1, n2) => {
    return n1 + n2;
};
console.log(adds(1, 2));
class Person {
    constructor(n) {
        this.name = n;
    }
    greet(phrase) {
        console.log(phrase + " " + this.name);
    }
}
let user1;
user1 = {
    name: "Junaedi",
    greet(phrase) {
        console.log(phrase + " " + this.name);
    },
};
user1 = new Person("Junaedi ramli");
user1.greet("Hai, nama saya");
