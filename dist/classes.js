"use strict";
console.log("Typescrypt course Class");
class Department {
    constructor(_id = "", _name = "") {
        this._id = _id;
        this._name = _name;
        this._employees = [];
    }
    static createEmploye(text) {
        return { name: text };
    }
    describe() {
        console.log("Department " + this._id + " : " + this._name);
    }
    addEmploye(employ) {
        this._employees.push(employ);
    }
    getEmployee() {
        console.log(this._employees.length);
        console.log(this._employees);
    }
}
Department.year = 2000;
class ITDepartment extends Department {
    constructor(_id, admins, report) {
        super(_id, "IT");
        this._admins = admins;
        this._lastReport = report[0];
    }
    get reportAdmin() {
        if (this._lastReport) {
            return this._lastReport;
        }
        throw new Error("Report not found");
    }
    set reportAdmin(value) {
        if (!value) {
            throw new Error("please add value");
        }
        this.addEmploye(value);
    }
    describe() {
        console.log("Id It Depatement is " + this._id);
    }
    addEmploye(name) {
        this._employees.push(name);
    }
}
const createE = Department.createEmploye("Junaedi");
const dept = new Department("1", "EDP");
const itdept = new ITDepartment("1", ["juna"], ["akan"]);
itdept.describe();
